// // import { render } from '@testing-library/react-native'
// // // import { ThemeProvider } from 'my-ui-lib'
// // // import { TranslationProvider } from 'my-i18n-lib'
// // // import defaultStrings from 'i18n/en-x-default'

// // const AllTheProviders = ({ children }) => {
// //   return (
// //     <ThemeProvider theme="light">
// //       <TranslationProvider messages={defaultStrings}>
// //         {children}
// //       </TranslationProvider>
// //     </ThemeProvider>
// //   )
// // }

// // const customRender = (ui, options) =>
// //   render(ui, { wrapper: AllTheProviders, ...options })

// // // re-export everything
// // export * from '@testing-library/react-native'

// // // override render method
// // export { customRender as render }



// // test-utils.js
// // import React from 'react'
// import { render as rtlRender } from '@testing-library/react-native'
// import { createStore } from 'redux'
// import { Provider } from 'react-redux'
// // Import your own reducer
// import reducer from '../reducer'

// const reducer = () => {};

// function render(
//   ui: React.ReactElement,
//   {
//     initialState,
//     store = createStore(reducer, initialState),
//     ...renderOptions
//   } = {}
// ) {
//   function Wrapper({ children }) {
//     return <Provider store={store}>{children}</Provider>
//   }
//   return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
// }

// // re-export everything
// export * from '@testing-library/react-native'
// // override render method
// export { render }
