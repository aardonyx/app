import { combineReducers, CombinedState, createStore, Store } from 'redux'
import { persistStore, persistReducer, PersistConfig, Persistor } from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import AsyncStorage from '@react-native-async-storage/async-storage'

import { UserState, userInitialState, userReducer } from './domains/User/user-redux'

type StoreState = CombinedState<{
    user: UserState
}>

const initialState: StoreState = {
    user: userInitialState,
}

const rootReducer = combineReducers({
    user: userReducer,
})

export type RootState = ReturnType<typeof rootReducer>

const persistConfig: PersistConfig<StoreState> = {
    key: 'root',
    storage: AsyncStorage,
    stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
}

export const useStore = (): [Store, Persistor] => {
    const store = createStore(
        persistReducer(persistConfig, rootReducer),
        initialState,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
    )
    const persistor = persistStore(store)

    return [store, persistor]
}
