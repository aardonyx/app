import { ApolloClient, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
// import { awsConfig } from '../app.config_OLD'
import { env } from './useAmplify'

const awsConfig = {
    API: {
        graphqlEndpoint: env.REACT_NATIVE_APPSYNC_GRAPHQL_ENDPOINT,
    },
}

export const useApollo = (): ApolloClient<NormalizedCacheObject> => {
    const client = new ApolloClient({
        uri: awsConfig.API.graphqlEndpoint,
        cache: new InMemoryCache(),
    })
    return client
}
