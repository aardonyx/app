import { Meal as GraphQLMeal } from '@aardonyx/base-team/types/__generated__/graphql'
import { Meal } from './meal'
import { HumanText } from './human-text'
import { Language } from './translations'

const LANG = Language.en

export const generate = (_meal: GraphQLMeal): string => {
    const meal = new Meal(_meal, LANG)
    const humanText = new HumanText(meal, LANG)

    let text = humanText.start()
    text += humanText.miseEnPlace()
    text += humanText.prepare()
    text += humanText.cook()
    text += humanText.end()

    return text
}
