import React from 'react'
import { View, Text, FlatList } from 'react-native'
import { getIcon } from '../components/Icon/Icon'

const testData = [
    {
        ingredient: 'rasped_old_cheese',
        amountPerPerson: 40,
        shopUrl: 'https://www.odinbezorgdienst.nl/pittig-geraspte-kaas.html',
        priceInCentsEUR: 199,
        unitPerProduct: 100,
        unitsNeeded: 80,
        orderAmount: 0.8,
    },
    {
        ingredient: 'eggplant',
        amountPerPerson: 0.5,
        shopUrl: 'https://www.odinbezorgdienst.nl/aubergine.html',
        priceInCentsEUR: 92,
        unitPerProduct: 1,
        unitsNeeded: 1,
        orderAmount: 1,
    },
    {
        ingredient: 'tomato',
        orderAmount: 0.4,
    },
    {
        ingredient: 'garlic',
        orderAmount: 1,
    },
    {
        ingredient: 'penne',
        orderAmount: 1,
    },
    {
        ingredient: 'rucola',
        orderAmount: 1,
    },
    {
        ingredient: 'tomato_passata',
        orderAmount: 1,
    },
    {
        ingredient: 'corn_salad',
        orderAmount: 1,
    },
]

// const ITEM_SIZE = 200

const Groceries = (): JSX.Element => {
    return (
        <View>
            <Text>Groceries</Text>
            <FlatList
                data={testData}
                // horizontal={true}
                // getItemLayout={(data, index) => ({ index, length: ITEM_SIZE, offset: ITEM_SIZE * index })}
                // style={{ height: 80 }}
                keyExtractor={(item) => item.ingredient}
                renderItem={({ item }) => (
                    <Text
                        style={{
                            // width: ITEM_SIZE,
                            // borderWidth: 5,
                            // borderColor: 'red'
                            height: 200,
                            fontSize: 30,
                        }}
                    >
                        {getIcon(item.ingredient, { height: 100, width: 100 })}
                        <Text>{Math.ceil(item.orderAmount)}x </Text>
                        <Text>{item.ingredient}</Text>
                    </Text>
                )}
            />
        </View>
    )
}

export default Groceries
