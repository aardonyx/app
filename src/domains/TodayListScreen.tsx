import React, { useState } from 'react'
import { useQuery } from '@apollo/client'
import { Text, FlatList } from 'react-native'
// import { SwipeRow } from 'native-base'
// import { S3Image } from 'aws-amplify-react-native'

// import { Ionicons } from '@expo/vector-icons'

import { LIST_MEALS } from '../services/GraphQL/queries/meal'
// import { Button } from '../theme/ui/Button'
import { listMeals } from '../__generated__/types/listMeals'
// import { TodayListScreenNavigationProps } from '../Router'
import { Button } from '../components/Button/Button'
import { Auth } from 'aws-amplify'
import { useDispatch } from 'react-redux'
import { userLogoutSuccessAction } from './User/user-redux'

const buttonStyle = { padding: 20 }

// type Props = React.PropsWithChildren<FC> & TodayListScreenNavigationProps

const TodayListScreen = (): JSX.Element => {
    const { loading, error, data, refetch } = useQuery<listMeals>(LIST_MEALS)
    const [isFetching, setIsFetching] = useState(false)
    const [user, setUser] = useState(undefined)
    const dispatch = useDispatch()

    if (!data) return <Text>Loading...A</Text>
    if (loading) return <Text>Loading...B</Text>
    if (error) return <Text>Error :(</Text>

    // const removeItem = (key) => {
    // data = data.filter((item) => item.key !== key)
    // setData({ data })
    // }

    const handleRefresh = () => {
        console.log('hello')
        setIsFetching(true)
        refetch()
        setIsFetching(false)
    }

    return (
        <React.Fragment>
            <Button
                style={buttonStyle}
                onPress={async () => {
                    const user = await Auth.currentAuthenticatedUser({
                        bypassCache: false,
                    })
                    setUser(user.username)
                    console.log(user)
                }}
            >
                <Text>Current User</Text>
            </Button>
            {user && <Text>Hello {user}</Text>}
            <Button
                style={buttonStyle}
                onPress={() => {
                    Auth.signOut()
                    dispatch(userLogoutSuccessAction())
                }}
            >
                <Text>Sign Out</Text>
            </Button>
            <FlatList
                contentContainerStyle={{
                    flex: 1,
                    alignSelf: 'stretch',
                    justifyContent: 'flex-start',
                }}
                keyExtractor={(item, index) => index.toString()}
                onRefresh={() => handleRefresh()}
                refreshing={isFetching}
                data={data.listMeals?.items}
                renderItem={({ item }) => (
                    //     <SwipeRow
                    //         style={{ height: 80 }}
                    //         leftOpenValue={75}
                    //         rightOpenValue={-75}
                    //         // left={
                    //         //     <Button variant="primary" onPress={() => alert()}>
                    //         //         <Ionicons name={'ios-add'} />
                    //         //     </Button>
                    //         // }
                    //         body={
                    //             <React.Fragment>
                    //                 <Button
                    //                     title={item ? item.title.en.toString() : ''}
                    //                     onPress={() => (item ? navigation.navigate('MealDetail', { meal: item }) : null)}
                    //                 >
                    //                     <Text

                    //                     // style={{ paddingLeft: 15 }}
                    //                     ></Text>
                    //                 </Button>
                    //                 {/* <S3Image imgKey={'test.jpg'} style={{ height: '100%', flexShrink: 1 }} /> */}
                    //             </React.Fragment>
                    //         }
                    //         // right={
                    //         //     <Button full danger onPress={() => removeItem(item?.__typename)}>
                    //         //         <Icon active name="trash" />
                    //         //     </Button>
                    // }
                    // />
                    <Text>{item?.title.en}</Text>
                )}
            />
        </React.Fragment>
    )
}

export default TodayListScreen
