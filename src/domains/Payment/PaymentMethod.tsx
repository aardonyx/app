import React from 'react'
import { Text } from 'react-native'

const PaymentMethod = (): JSX.Element => {
    return (
        <Text>
            + Toevoegen Select creditcard, SEPA or Paypal. Zodra u op toevoegen klikt wordt u doorgestuurd naar Paypal
            om de betaling goed te keuren.
        </Text>
    )
}

export default PaymentMethod
