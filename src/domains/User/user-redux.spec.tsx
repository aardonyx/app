import { userReducer } from './user-redux'

describe('user reducer', () => {
    //   it('should return the initial state', () => {
    //     expect(userReducer(undefined, {})).toEqual([
    //       {
    //         text: 'Use Redux',
    //         completed: false,
    //         id: 0
    //       }
    //     ])
    //   })
    it('should handle user login success', () => {
        expect(userReducer(undefined, { type: 'User/Login/Success' })).toEqual({
            isError: false,
            isLoading: false,
            isSignedIn: true,
        })
    })

    //   it('should handle ADD_TODO', () => {
    //     expect(
    //       reducer([], {
    //         type: types.ADD_TODO,
    //         text: 'Run the tests'
    //       })
    //     ).toEqual([
    //       {
    //         text: 'Run the tests',
    //         completed: false,
    //         id: 0
    //       }
    //     ])

    //     expect(
    //       reducer(
    //         [
    //           {
    //             text: 'Use Redux',
    //             completed: false,
    //             id: 0
    //           }
    //         ],
    //         {
    //           type: types.ADD_TODO,
    //           text: 'Run the tests'
    //         }
    //       )
    //     ).toEqual([
    //       {
    //         text: 'Run the tests',
    //         completed: false,
    //         id: 1
    //       },
    //       {
    //         text: 'Use Redux',
    //         completed: false,
    //         id: 0
    //       }
    //     ])
    //   })
})
