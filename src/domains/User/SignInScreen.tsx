import React from 'react'
import { View, Text } from 'react-native'
import { Auth, Cache, Hub } from 'aws-amplify'
import { CognitoHostedUIIdentityProvider } from '@aws-amplify/auth'

import { Button } from '../../components/Button/Button'
// import { useDispatch } from 'react-redux'
// import { userLoginSuccessAction } from './user-redux'

const buttonStyle = { padding: 20 }

export const SignInScreen = (): JSX.Element => {
    // const [user, setUser] = useState(undefined)
    // const dispatch = useDispatch()

    return (
        <View style={{ marginTop: 50 }}>
            <Button
                style={buttonStyle}
                onPress={() => {
                    Auth.federatedSignIn().then((value) => {
                        console.log(value)
                        Cache.setItem('federatedInfo', value)
                    })
                }}
            >
                <Text>Sign In - Hosted UI</Text>
            </Button>
            <Button
                style={buttonStyle}
                onPress={() => Auth.federatedSignIn({ provider: CognitoHostedUIIdentityProvider.Google })}
            >
                <Text>Sign In - Google</Text>
            </Button>
            <Button style={buttonStyle} onPress={async () => await Auth.signIn('aardonyx1@gmail.com', 'aardonyx1')}>
                <Text>Sign In - Username/Password</Text>
            </Button>
            <Button
                testID="stepOne"
                style={buttonStyle}
                onPress={async () => {
                    const domain = 'https://aardonyx-base-team.herokuapp.com'
                    // const domain = 'https://localhost:8443'
                    const response = await fetch(domain + '/oauth2/token', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({ code: '7573657231' }),
                    })
                    const result = await response.json()

                    Hub.dispatch('auth', { event: 'mockSignIn', data: { result } })

                    // const federatedInfo = localStorage.getItem("aws-amplify-cachefederatedInfo");

                    // const federatedInfo = Cache.getItem('federatedInfo');
                    // console.log(Cache.getAllKeys());
                    // console.log(federatedInfo)
                    // const challengeResponse = "the answer for the challenge";
                    // const username = 'test'
                    // const password = 'pass'

                    // Auth.signIn(username, password)
                    //     .then(user => {
                    //         if (user.challengeName === 'CUSTOM_CHALLENGE') {
                    //             // to send the answer of the custom challenge
                    //             Auth.sendCustomChallengeAnswer(user, challengeResponse)
                    //                 .then(user => console.log(user))
                    //                 .catch(err => console.log(err));
                    //         } else {
                    //             console.log(user);
                    //         }
                    //     })
                    //     .catch(err => console.log(err));
                    // }
                }}
            >
                <Text>Custom Auth</Text>
            </Button>
            <Button style={buttonStyle} onPress={async () => console.log(await Auth.currentSession())}>
                <Text>Current Session</Text>
            </Button>
            <Button style={buttonStyle} onPress={async () => console.log(await Auth.currentCredentials())}>
                <Text>Current Credentials</Text>
            </Button>
            <Button
                style={buttonStyle}
                onPress={async () => {
                    const user = await Auth.currentAuthenticatedUser({
                        bypassCache: false,
                    })
                    // setUser(user)
                    console.log(user)
                }}
            >
                <Text>Current User</Text>
            </Button>
            <Button style={buttonStyle} onPress={() => Auth.signOut()}>
                <Text>Sign Out</Text>
            </Button>
            {/* { user && <Text>Hello, { user.username } </Text> } */}
        </View>
    )
}
