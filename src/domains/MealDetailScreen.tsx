import React, { FC } from 'react'
import { View, Text } from 'react-native'
import { generate } from '../_temp/lib/generate-recipe'
import { MealDetailScreenNavigationProps } from '../Router'

type Props = React.PropsWithChildren<FC> & MealDetailScreenNavigationProps

const MealDetailScreen = ({ route }: Props): JSX.Element => {
    return (
        <View>
            <Text>{generate(route.params.meal)}</Text>
        </View>
    )
}

export default MealDetailScreen
