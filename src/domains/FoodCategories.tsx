/*
Voedselcategorien

Groente
- Tomaten, komkomers
- Paprika en pepers
- Courgette, aubergine
- Bladgroenten
- Kool
- Peulvruchten
- Wortel, knollen
- Ui, knoflook
- Aardappelen
- Paddenstoelen
- Kiemgroenten
- Verse kruiden, specerijen
- Groenteconserven

Fruit
- Appel, peer
- Bananen
- Avocado
- Citrusvruchten
- Zacht fruit
- Exotisch fruit
- Diepvries fruit
- Gedroogd fruit
- Fruitconserven

Zuivel, desserts, eieren
- Plantaardige zuivel

Bakkerij
Pasta, rijst, internationaal
Olie, sauzen, specerijen, bouillon

Kaas:
- Half hard, witte kort, blauw, rode korts, gieten/schapen, feta, mozzarella, harde kazen
- Smeer- en roomkaas
- Geraspte kaas
- Plakken kaas (jong, jong belegen, belegen, extra belegen, oud)
- Stukken kaas
- Plantaardige kaas

Delicatessen
- Olijven
- Tapas
- Noten
- Gedroogd fruit
- Tafelzuren

*/