import * as React from 'react'
import Svg, { SvgProps, Path } from 'react-native-svg'

function SvgHome(props: SvgProps) {
    return (
        <Svg viewBox="0 0 16 15" fill="none" {...props}>
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M0.707107 8.48528L1.77817 7.41421V13.0563V13.0564V14.0563H13.7782V13.0564V13.0563V7.41421L14.8492 8.48528L15.5563 7.77817L8.48528 0.707107L7.77817 0L7.07107 0.707107L0 7.77817L0.707107 8.48528ZM2.77817 13.0563V6.41421L7.77817 1.41421L12.7782 6.41421V13.0563H9.77817V9.05638V8.05638H5.77817V9.05638V13.0563H2.77817ZM6.77817 9.05638V13.0563H8.77817V9.05638H6.77817Z"
                fill="#000"
                fillOpacity={0.8}
            />
        </Svg>
    )
}

export default SvgHome
