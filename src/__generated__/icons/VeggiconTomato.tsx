import * as React from 'react'
import Svg, { SvgProps, Path } from 'react-native-svg'

function SvgVeggiconTomato(props: SvgProps) {
    return (
        <Svg viewBox="0 0 16 16" fill="none" {...props}>
            <Path
                d="M15.5 7.9C15.7 11.75 12.225 15 8.025 15C3.825 15 0.1 11.55 0.525 7.725C1.125 2.35 5.1 2 8.025 2C9.8 2 15.05 0.45 15.5 7.9Z"
                fill="#EF4D3C"
            />
            <Path
                d="M2.75 6.75C4.3 4.35 6.95 5.05 7.65 4.15C7.65 5.875 8.9 5.025 9.525 5.8C10.325 6.8 10.625 8.575 11.575 8.925C10.65 6.95 12.15 7.025 10.05 4.375C11.175 5.075 11.75 4.375 13.275 5C11.95 2.9 9.875 3.5 9.875 3.5C9.875 3.5 11.175 2.3 12.275 2.925C11.125 1.225 7.8 3.375 7.8 3.375C7.8 3.375 6.425 1.025 3.475 3.5C5.2 2.8 7.1 3.5 7.1 3.5C7.1 3.5 3.975 2.725 2.75 6.75Z"
                fill="#8CC63E"
            />
            <Path d="M2.75 6.75C2.75 6.75 4.575 3.375 7.725 3.675C4.95 2.4 2.75 5.1 2.75 6.75Z" fill="#64892F" />
            <Path
                d="M3.475 3.5C3.475 3.5 5.775 1.775 7.8 3.5 7.2 1.4 4.125 2.375 3.475 3.5zM8.3 3.725C11.35 5.125 10.325 6.725 11.575 8.975 10.725 7.1 12.825 3.725 8.3 3.725z"
                fill="#64892F"
            />
            <Path
                d="M7.1 3.5C7.1 3.5 7.8 2.45 8.05 1.15C8.075 0.975 8.825 1 8.8 1.175C8.55 2.85 9.225 3.775 9.225 3.775C9.225 3.775 8.7 3.95 8.05 3.95C7.4 3.975 7.1 3.5 7.1 3.5Z"
                fill="#64892F"
            />
            <Path
                d="M8.425 1.3C8.63211 1.3 8.8 1.23284 8.8 1.15C8.8 1.06716 8.63211 1 8.425 1C8.21789 1 8.05 1.06716 8.05 1.15C8.05 1.23284 8.21789 1.3 8.425 1.3Z"
                fill="#8CC63E"
            />
        </Svg>
    )
}

export default SvgVeggiconTomato
