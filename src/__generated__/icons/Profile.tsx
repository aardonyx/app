import * as React from 'react'
import Svg, { SvgProps, Path, Circle } from 'react-native-svg'

function SvgProfile(props: SvgProps) {
    return (
        <Svg viewBox="0 0 12 17" fill="none" {...props}>
            <Path
                d="M6.21739 9.47827C3.33591 9.47827 1 11.8142 1 14.6957V16H11.4348V14.6957C11.4348 11.8142 9.09888 9.47827 6.21739 9.47827Z"
                stroke="#000"
            />
            <Circle cx={6.218} cy={4.913} stroke="#000" strokeLinecap="square" r={3.913} />
        </Svg>
    )
}

export default SvgProfile
