/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MeasurementUnit, Equipment, Alias, InstructionType, Action, MealServeTiming } from './globalTypes'

// ====================================================
// GraphQL query operation: listMeals
// ====================================================

export interface listMeals_listMeals_items_title {
    __typename: 'Translation'
    en: string
    nl: string
}

export interface listMeals_listMeals_items_amounts_ingredient_name {
    __typename: 'Translation'
    en: string
    nl: string
}

export interface listMeals_listMeals_items_amounts_ingredient {
    __typename: 'Ingredient'
    _id: any
    name: listMeals_listMeals_items_amounts_ingredient_name
    priceInEurocents: number | null
    unit: MeasurementUnit
}

export interface listMeals_listMeals_items_amounts {
    __typename: 'MealAmounts'
    amountPerPerson: number
    ingredient: listMeals_listMeals_items_amounts_ingredient
}

export interface listMeals_listMeals_items_equipment {
    __typename: 'MealEquipment'
    equipment: Equipment
    as: Alias | null
}

export interface listMeals_listMeals_items_instructions_actions {
    __typename: 'InstructionActions'
    timerAt: number
    action: Action
}

export interface listMeals_listMeals_items_instructions_ingredients_ingredient_name {
    __typename: 'Translation'
    en: string
    nl: string
}

export interface listMeals_listMeals_items_instructions_ingredients_ingredient {
    __typename: 'Ingredient'
    _id: any
    name: listMeals_listMeals_items_instructions_ingredients_ingredient_name
    priceInEurocents: number | null
    unit: MeasurementUnit
}

export interface listMeals_listMeals_items_instructions_ingredients {
    __typename: 'InstructionIngredients'
    order: number
    amountDevided: number
    use: string | null
    ingredient: listMeals_listMeals_items_instructions_ingredients_ingredient | null
}

export interface listMeals_listMeals_items_instructions {
    __typename: 'Instruction'
    id: string
    type: InstructionType | null
    order: number | null
    timerInSeconds: number | null
    use: string | null
    as: Alias | null
    actions: listMeals_listMeals_items_instructions_actions[]
    ingredients: (listMeals_listMeals_items_instructions_ingredients | null)[] | null
}

export interface listMeals_listMeals_items_serve {
    __typename: 'MealServe'
    timing: MealServeTiming
    use: string
}

export interface listMeals_listMeals_items {
    __typename: 'Meal'
    _id: any
    /**
     * translations: Translation!
     */
    title: listMeals_listMeals_items_title
    amounts: listMeals_listMeals_items_amounts[]
    equipment: listMeals_listMeals_items_equipment[]
    instructions: listMeals_listMeals_items_instructions[]
    serve: listMeals_listMeals_items_serve[]
}

export interface listMeals_listMeals {
    __typename: 'MealConnection'
    items: (listMeals_listMeals_items | null)[] | null
}

export interface listMeals {
    listMeals: listMeals_listMeals | null
}
