/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum Action {
    bake = 'bake',
    boil = 'boil',
    cook = 'cook',
    cutCubesLarge = 'cutCubesLarge',
    cutCubesNormal = 'cutCubesNormal',
    cutCubesSmall = 'cutCubesSmall',
    cutFine = 'cutFine',
    cutQuarts = 'cutQuarts',
    cutSlicesNormal = 'cutSlicesNormal',
    cutSlicesThick = 'cutSlicesThick',
    cutSlicesThin = 'cutSlicesThin',
    drain = 'drain',
    flavorPeper = 'flavorPeper',
    flavorPeperAndSalt = 'flavorPeperAndSalt',
    flavorSalt = 'flavorSalt',
    flip = 'flip',
    fryHard = 'fryHard',
    fryNormal = 'fryNormal',
    frySoft = 'frySoft',
    grease = 'grease',
    heat_220_celcius = 'heat_220_celcius',
    layer = 'layer',
    layerLeft = 'layerLeft',
    layerRight = 'layerRight',
    layers = 'layers',
    mix = 'mix',
    press = 'press',
    rasp = 'rasp',
    rinse = 'rinse',
    sprinkleBothSides = 'sprinkleBothSides',
    steamOut = 'steamOut',
    tear = 'tear',
}

export enum Alias {
    dressing = 'dressing',
    garnish = 'garnish',
    pastaPan = 'pastaPan',
    preparedOvenDish = 'preparedOvenDish',
    salad = 'salad',
    saladBowl = 'saladBowl',
    sauce = 'sauce',
    sauceBowl = 'sauceBowl',
}

export enum Equipment {
    bowlLarge = 'bowlLarge',
    bowlNormal1 = 'bowlNormal1',
    bowlNormal2 = 'bowlNormal2',
    bowlSmall = 'bowlSmall',
    cookingPanWithCover = 'cookingPanWithCover',
    cutboard = 'cutboard',
    fryingPan = 'fryingPan',
    oven = 'oven',
    ovenDish = 'ovenDish',
    plateNormal = 'plateNormal',
    tableFork = 'tableFork',
    tableSpoon = 'tableSpoon',
}

export enum InstructionType {
    cook = 'cook',
    cut = 'cut',
    prepare = 'prepare',
}

export enum MealServeTiming {
    after = 'after',
    before = 'before',
    during = 'during',
}

export enum MeasurementUnit {
    clove = 'clove',
    gram = 'gram',
    leaves = 'leaves',
    milliliter = 'milliliter',
    piece = 'piece',
    tableSpoon = 'tableSpoon',
    teaSpoon = 'teaSpoon',
}

//==============================================================
// END Enums and Input Objects
//==============================================================
