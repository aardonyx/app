/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MeasurementUnit } from './globalTypes'

// ====================================================
// GraphQL fragment: AmountsFragment
// ====================================================

export interface AmountsFragment_ingredient_name {
    __typename: 'Translation'
    en: string
    nl: string
}

export interface AmountsFragment_ingredient {
    __typename: 'Ingredient'
    _id: any
    name: AmountsFragment_ingredient_name
    priceInEurocents: number | null
    unit: MeasurementUnit
}

export interface AmountsFragment {
    __typename: 'MealAmounts'
    amountPerPerson: number
    ingredient: AmountsFragment_ingredient
}
