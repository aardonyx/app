/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { InstructionType, Alias, Action, MeasurementUnit } from './globalTypes'

// ====================================================
// GraphQL fragment: InstructionsFragment
// ====================================================

export interface InstructionsFragment_actions {
    __typename: 'InstructionActions'
    timerAt: number
    action: Action
}

export interface InstructionsFragment_ingredients_ingredient_name {
    __typename: 'Translation'
    en: string
    nl: string
}

export interface InstructionsFragment_ingredients_ingredient {
    __typename: 'Ingredient'
    _id: any
    name: InstructionsFragment_ingredients_ingredient_name
    priceInEurocents: number | null
    unit: MeasurementUnit
}

export interface InstructionsFragment_ingredients {
    __typename: 'InstructionIngredients'
    order: number
    amountDevided: number
    use: string | null
    ingredient: InstructionsFragment_ingredients_ingredient | null
}

export interface InstructionsFragment {
    __typename: 'Instruction'
    id: string
    type: InstructionType | null
    order: number | null
    timerInSeconds: number | null
    use: string | null
    as: Alias | null
    actions: InstructionsFragment_actions[]
    ingredients: (InstructionsFragment_ingredients | null)[] | null
}
