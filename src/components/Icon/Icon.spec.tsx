import * as Icons from '../../__generated__/icons'
import { getIcon } from './Icon'

describe('getIcon', () => {
    Object.keys(Icons).map((key) => {
        const ingredient = key
            .replace('Veggicon', '') // remove prefix
            .match(/[A-Z][a-z]+/g) // split by each PascalCase uppercased character
            ?.map((part) => part.toLowerCase()) // make each part lowercase
            .join('_')

        if (ingredient) {
            it(`should return for ${ingredient}`, () => {
                const Icon = getIcon(ingredient, {})
                expect(JSON.stringify(Icon)).not.toContain('not_found')
            })
        }
    })
})
