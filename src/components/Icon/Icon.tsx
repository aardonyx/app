import React from 'react'
import { Text, StyleProp, ViewStyle } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import * as Icon from '../../__generated__/icons'

export const setIcon = (name: string, size: number): JSX.Element => <Ionicons body={name} size={size} />

export const getIcon = (name: string, style: StyleProp<ViewStyle>): JSX.Element => {
    switch (name) {
        case 'home':
            return <Icon.Home style={style} />
        case 'profile':
            return <Icon.Profile style={style} />
        case 'rasped_old_cheese':
            return <Icon.VeggiconRaspedOldCheese style={style} />
        case 'eggplant':
            return <Icon.VeggiconEggplant style={style} />
        case 'tomato':
            return <Icon.VeggiconTomato style={style} />
        case 'garlic':
            return <Icon.VeggiconGarlic style={style} />
        case 'penne':
            return <Icon.VeggiconPenne style={style} />
        case 'rucola':
            return <Icon.VeggiconRucola style={style} />
        case 'tomato_passata':
            return <Icon.VeggiconTomatoPassata style={style} />
        case 'corn_salad':
            return <Icon.VeggiconCornSalad style={style} />
    }
    return <Text>not_found</Text>
}
