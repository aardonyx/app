// import React from 'react'
// import { TouchableOpacityProps, Text } from 'react-native'
// // import { Button as ThemeButton, Variant } from '@aardonyx/ui/app'
// // import styled, { DefaultTheme } from 'styled-components/native'
import { TouchableOpacity as RNButton } from 'react-native'
// type ThemeButton = Omit<TouchableOpacityProps, 'style'>

// interface Props extends ThemeButton {
//     variant: Variant
//     // theme: DefaultTheme
//     children: JSX.Element[] | JSX.Element
// }
export const Button = RNButton
// const ModifiedThemeButton = (props: Props) => (
//     <ThemeButton {...props}>
//         <Text>My</Text>
//         {props.children}
//     </ThemeButton>
// )
// // export const Button = styled(ModifiedThemeButton)`
// //     border: 3px solid ${(props) => props.theme.brandExtra};
// // `
// export const Button = ModifiedThemeButton;
