import * as React from 'react'
import { useSelector } from 'react-redux'
import {
    CompositeNavigationProp,
    NavigationContainer,
    NavigatorScreenParams,
    RouteProp,
} from '@react-navigation/native'
import { createStackNavigator, StackNavigationProp, StackScreenProps } from '@react-navigation/stack'
import { BottomTabNavigationProp, createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Meal as GraphQLMeal } from '@aardonyx/base-team/types/__generated__/graphql'

import MealDetailScreen from './domains/MealDetailScreen'
import TodayListScreen from './domains/TodayListScreen'
import { SignInScreen } from './domains/User/SignInScreen'
// import Schedule from './domains/Schedule'
// import Groceries from './domains/Groceries'
// import Settings from './domains/Settings'
import { selectUser } from './domains/User/user-redux'

import { setIcon } from './components/Icon/Icon'

export const RootNavigator = (): JSX.Element => {
    const user = useSelector(selectUser)

    return <NavigationContainer>{user.isSignedIn ? <AppTabNavigator /> : <AuthStackNavigator />}</NavigationContainer>
}

const AuthStackNavigator = () => {
    const Stack = createStackNavigator()

    return (
        <Stack.Navigator>
            <Stack.Screen name="SignIn" component={SignInScreen} />
        </Stack.Navigator>
    )
}

type AuthStackNavigatorParamList = {
    SignIn: undefined
}

export type SignInScreenNavigationProps = StackScreenProps<AuthStackNavigatorParamList, 'SignIn'>

const AppTabNavigator = () => {
    const Tab = createBottomTabNavigator()

    return (
        // <React.Fragment></React.Fragment>
        <Tab.Navigator initialRouteName="Home">
            {/* <Tab.Navigator> */}
            <Tab.Screen
                name="Home"
                options={{ tabBarIcon: ({ size }) => setIcon('ios-pizza', size) }}
                component={HomeStackNavigator}
            />
            {/*
            <Tab.Screen
                name="Schedule"
                component={Schedule}
                options={{ tabBarIcon: ({ size }) => setIcon('ios-calendar', size) }}
            />
            <Tab.Screen
                name="Groceries"
                component={Groceries}
                options={{ tabBarIcon: ({ size }) => setIcon('ios-cart', size) }}
            /> */}
            {/* <Tab.Screen
                name="Settings"
                component={Settings}
                // options={{ tabBarBadge: 3, tabBarIcon: ({ size }) => setIcon('ios-cog', size) }}
            /> */}
        </Tab.Navigator>
    )
}

type AppTabNavigatorParamList = {
    Home: NavigatorScreenParams<HomeStackNavigatorParamList>
    Settings: { setting: boolean }
}

const HomeStackNavigator = () => {
    const Stack = createStackNavigator()

    return (
        <Stack.Navigator initialRouteName="TodayList" screenOptions={{ animationEnabled: false }}>
            <Stack.Screen name="TodayList" component={TodayListScreen} />
            <Stack.Screen name="MealDetail" component={MealDetailScreen} />
        </Stack.Navigator>
    )
}

type HomeStackNavigatorParamList = {
    TodayList: { something: string }
    MealDetail: { meal: GraphQLMeal }
}

export type MealDetailScreenNavigationProps = {
    navigation: CompositeNavigationProp<
        StackNavigationProp<HomeStackNavigatorParamList, 'MealDetail'>,
        BottomTabNavigationProp<AppTabNavigatorParamList>
    >
    route: RouteProp<HomeStackNavigatorParamList, 'MealDetail'>
}

export type TodayListScreenNavigationProps = {
    navigation: CompositeNavigationProp<
        StackNavigationProp<HomeStackNavigatorParamList, 'TodayList'>,
        BottomTabNavigationProp<AppTabNavigatorParamList>
    >
    route: RouteProp<HomeStackNavigatorParamList, 'TodayList'>
}
