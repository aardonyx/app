import React from 'react'
import * as reactRedux from 'react-redux'
import { render } from '@testing-library/react-native'
// import { render, cleanup, act } from '../utils/test-utils'
import { persistStore } from 'redux-persist'

import App from './App'
import { UserActions, userInitialState, UserState } from './domains/User/user-redux'
import { CombinedState, combineReducers, createStore, Reducer } from 'redux'

type StoreState = CombinedState<{
    user: UserState
}>
const initialState: StoreState = {
    user: userInitialState,
}
const userReducer: Reducer<UserState, UserActions> = (user, action): UserState => {
    switch (action.type) {
        default:
            return user || userInitialState
    }
}
const rootReducer = combineReducers({
    user: userReducer,
})
const store = createStore(rootReducer, initialState)
const persistor = persistStore(store)

jest.mock('./useStore', () => {
    return { useStore: () => [store, persistor] }
})

describe('<App />', () => {
    const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
    const useStateMock = jest.spyOn(React, 'useState')
    const setIsReady = jest.fn()

    beforeEach(() => jest.useFakeTimers())
    afterEach(() => jest.useRealTimers())
    beforeEach(() => {
        useSelectorMock.mockClear()
        useStateMock.mockClear()
    })
    // afterEach(() => cleanup)

    describe('when not ready', () => {
        it('does not render anything', async () => {
            useStateMock.mockImplementation(() => [false, setIsReady])

            const component = <App />
            const { toJSON } = render(component)
            expect(toJSON()).toEqual(null)
        })
    })

    describe('when ready', () => {
        beforeEach(() => {
            useStateMock.mockImplementation(() => [true, setIsReady])
        })

        it('loads signin screen when not authenticated', () => {
            useSelectorMock.mockReturnValue({ isSignedIn: false })

            const component = <App />
            const { toJSON, getByText } = render(component)

            expect(getByText(/Sign In - Google/i)).toBeDefined()
            expect(toJSON()).toMatchSnapshot()
        })

        it('loads home screen when logged in', () => {
            useSelectorMock.mockReturnValue({ isSignedIn: true })

            const component = <App />
            // const { toJSON } = await waitFor(() => render(component))
            const { toJSON, getByText } = render(component)

            expect(getByText(/Home/)).toBeDefined()
            expect(toJSON()).toMatchSnapshot()
        })
    })
})
