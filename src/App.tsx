import React, { useState } from 'react'
import { Provider as StoreProvider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
// import { ThemeProvider } from 'styled-components/native'
import { ApolloProvider } from '@apollo/client'
// import { AppLoading } from 'expo'
import AppLoading from 'expo-app-loading'

import { RootNavigator } from './Router'

import { AmplifyProvider } from './useAmplify'
// import { useStorage } from './useStorage'
// import { useHub } from './useHub'
import { useApollo } from './useApollo'
import { useStore } from './useStore'
// import { getTheme } from './theme'

const App: React.FC = () => {
    const [isReady, setIsReady] = useState(false)

    const client = useApollo()
    const [store, persistor] = useStore()

    const loadAsync = async () => {
        Promise.all([true])
    }

    if (!isReady) {
        return <AppLoading startAsync={loadAsync} onFinish={() => setIsReady(true)} onError={console.warn} />
    }

    return (
        <StoreProvider store={store}>
            <AmplifyProvider>
                <ApolloProvider client={client}>
                    {/* <ThemeProvider theme={getTheme({ mode: 'light' })}> */}
                    <PersistGate loading={null} persistor={persistor}>
                        <RootNavigator />
                    </PersistGate>
                    {/* </ThemeProvider> */}
                </ApolloProvider>
            </AmplifyProvider>
        </StoreProvider>
    )
}

export default App
