import { gql } from '@apollo/client'

// export const MEAL_FRAGMENTS = gql`

// `
export const LIST_MEALS = gql`
    fragment AmountsFragment on MealAmounts {
        amountPerPerson
        ingredient {
            _id
            name {
                en
                nl
            }
            priceInEurocents
            unit
        }
    }
    fragment InstructionsFragment on Instruction {
        id
        type
        order
        timerInSeconds
        use
        as
        actions {
            timerAt
            action
        }
        ingredients {
            order
            amountDevided
            use
            ingredient {
                _id
                name {
                    en
                    nl
                }
                priceInEurocents
                unit
            }
        }
    }
    query listMeals {
        listMeals {
            items {
                _id
                title {
                    en
                    nl
                }
                amounts {
                    ...AmountsFragment
                }
                equipment {
                    equipment
                    as
                }
                instructions {
                    ...InstructionsFragment
                }
                serve {
                    timing
                    use
                }
            }
        }
    }
`
