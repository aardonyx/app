// / WARNING THIS ISN'T VERSIONED
import { ExpoConfig, ConfigContext } from '@expo/config';

// const LOCALHOST_VIA_NGROK = '';

const env = {
    // REACT_NATIVE_APPSYNC_GRAPHQL_ENDPOINT: 'https://localhost:8443/graphql',
    REACT_NATIVE_APPSYNC_GRAPHQL_ENDPOINT: 'https://aardonyx-base-team.herokuapp.com/graphql',
    REACT_NATIVE_APPSYNC_REGION: 'localhost_region',
    REACT_NATIVE_AWS_COGNITO_REGION: 'us-east-1',
    REACT_NATIVE_AWS_IDENTITY_POOL_ID: 'localost_identity_pool_id',
    REACT_NATIVE_AWS_USER_POOLS_ID: 'localhost_user_pool_id',
    REACT_NATIVE_AWS_USER_POOLS_WEB_CLIENT_ID: 'localhost_admin_client',
    // REACT_NATIVE_OAUTH_DOMAIN: 'localhost:8443',
    REACT_NATIVE_OAUTH_DOMAIN: 'aardonyx-base-team.herokuapp.com',
    // REACT_NATIVE_OAUTH_REDIRECT_SIGN_IN: 'http://localhost:19006/',
    REACT_NATIVE_OAUTH_REDIRECT_SIGN_IN: 'exp://192.168.1.108:19000',    
    // REACT_NATIVE_OAUTH_REDIRECT_SIGN_OUT: 'http://localhost:19006/',
    REACT_NATIVE_OAUTH_REDIRECT_SIGN_OUT: 'exp://192.168.1.108:19000',
}

export default ({ config }: ConfigContext): Partial<ExpoConfig> => {
    return {
        ...config,
        // extra: {
        //     aws: awsConfig
        // }
    };
};

export const awsConfig = {
    Auth: {
        identityPoolId: env.REACT_NATIVE_AWS_IDENTITY_POOL_ID,
        region: env.REACT_NATIVE_AWS_COGNITO_REGION,
        userPoolId: env.REACT_NATIVE_AWS_USER_POOLS_ID,
        userPoolWebClientId: env.REACT_NATIVE_AWS_USER_POOLS_WEB_CLIENT_ID,
        authenticationFlowType: 'USER_SRP_AUTH', // 'USER_SRP_AUTH' (Secure Remote Password protocol) //'USER_PASSWORD_AUTH' (sends plain credentials to backend for verification) //'CUSTOM_AUTH' (for other purposes)
        oauth: {
            domain: env.REACT_NATIVE_OAUTH_DOMAIN,
            // domain: '0a6cf7be4ec8.ngrok.io',
            scope: [
                'email',
                'openid',
                'profile'
            ],
            redirectSignIn: env.REACT_NATIVE_OAUTH_REDIRECT_SIGN_IN,
            redirectSignOut: env.REACT_NATIVE_OAUTH_REDIRECT_SIGN_OUT,
            responseType: 'code',
            urlOpenener: async (url: string, redirectUrl: string): Promise<unknown> => url + redirectUrl
        },
    },
    API: {
        graphqlEndpoint: env.REACT_NATIVE_APPSYNC_GRAPHQL_ENDPOINT,
        // graphqlEndpoint: 'https://a4060bc296bb.ngrok.io/graphql',
        // graphqlEndpoint: 'https://localhost:8443/graphql',
        // region: 'us-east-1',
        // authenticationType: 'AMAZON_COGNITO_USER_POOLS', // You have configured Auth with Amazon Cognito User Pool ID and Web Client Id
    },
    Storage: {
        AWSS3: {
            bucket: 'localhost_bucket',
            region: 'eu-west-1',
            dangerouslyConnectToHttpEndpointForTesting: true // makes s3 api calls to http://localhost:20005/
        }
    }
};