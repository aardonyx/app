/* eslint-disable no-undef */
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
module.exports = {
    client: {
        addTypename: false,
        service: {
            name: 'aardonyx-base-team-graphql',
            // url: 'https://localhost:8443/graphql'
            url: 'https://aardonyx-base-team.herokuapp.com/graphql'
        }
    },
};
